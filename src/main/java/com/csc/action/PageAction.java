package com.csc.action;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Properties;

import org.apache.log4j.Logger;

import com.csc.api.ActionAPI;
import com.csc.fixture.InteractWebApplication;
import com.csc.fixture.SetUpConfiguration;

public class PageAction {
	private static org.apache.log4j.Logger log = Logger.getLogger(InteractWebApplication.class);
	ActionAPI api = new ActionAPI();


	public ArrayList<String> readLocator(String locator)  {
		ArrayList<String> list = null;
		Properties prop = new Properties();
		try {
		    //load a properties file from class path, inside static method
			String file = "C:/Users/training//Desktop/FitNesse/FitNesseRoot/bin/locator.properties";
			String filename = SetUpConfiguration.configuration.getUrl() + "/" + SetUpConfiguration.configuration.getFileConfig();
 		    prop.load(new FileInputStream(file));
 		   
		    String str = prop.getProperty(locator);
		    String[] words =  str.split("=>");
		    String type = words[0];
		    String val = words[1];

		    list = new ArrayList<String>();
		    list.add(type);
		    list.add(val);
		} 
		catch (IOException ex) {
		    ex.printStackTrace();
		}
		return list;
	  }
	

	

	public void iNavigateToPage(String url) {		
		System.out.println("Page Navigate");
		api.toNavigate(url);
		log.info("iNavigateToPage: ");
	}

	public void iClickTheElement(String locator) throws InterruptedException {
		ArrayList<String> list = readLocator(locator);		
		api.toClick(list.get(0), list.get(1));
		log.info("iClickTheElement: ");
	}

	public void iShouldSeeThePopupAppears(String windowId) {
		api.toShowPopUp(windowId);
		log.info("iShouldSeeThePopupAppears: ");
	}
	
	public void iOpenThePopupWindowWith(String url, String windowId) {
		api.toOpenPopUpWithUrl(url, windowId);
		log.info("iOpenThePopupWindowWith: ");
	}
	
	public void iCloseTheBrowser(){
		api.toCloseBrowser();
		log.info("iCloseTheBrowser: ");
	}
	
	public void iDoubleClickTheElement(String locator){
		ArrayList<String> list = readLocator(locator);		
		api.toDoubleClick(list.get(0), list.get(1));
		log.info("iDoubleClickTheElement: ");
	}
	
	public void iDragTheElementAndDropToTheElement(String locator1, String locator2){
		ArrayList<String> list1 = readLocator(locator1);
		ArrayList<String> list2 = readLocator(locator2);
		api.toDragAndDrop(list1.get(0),list1.get(1),list2.get(0),list2.get(1));
		log.info("iDragTheElementAndDropToTheElement: ");
	}
	
	public void iMoveTheFocusToTheElement(String locator){
		ArrayList<String> list = readLocator(locator);		
		api.toMoveTheFocusToElement(list.get(0), list.get(1));
		log.info("iMoveTheFocusToTheElement: ");
	}
	
	public void iPressTheLeftMouseButtonOnTheElement(String locator){
		ArrayList<String> list = readLocator(locator);		
		api.toPressLeftMouse(list.get(0), list.get(1));
		log.info("iPressTheLeftMouseButtonOnTheElement: ");
	}

	public void iReleaseTheLeftMouseButtonOnTheElement(String locator){
		ArrayList<String> list = readLocator(locator);		
		api.toReleaseMouse(list.get(0), list.get(1));
		log.info("iReleaseTheLeftMouseButtonOnTheElement: ");
	}
	
	public void iPause(String waitTime){
		api.toPause(waitTime);
		log.info("iPause: ");
	}
	
	public void iRefresh(){
		api.toRefresh();
		log.info("iRefresh: ");
	}
	
	public void iSelectTheOptionFromADropDown(String optionLocator, String input){
		ArrayList<String> list = readLocator(optionLocator);
		api.toSelectDropdownList(list.get(0),list.get(1),input);
		log.info("iSelectTheOptionFromADropDown: ");
	}
	
	public void iSelectTheWindow(String windowId){
		api.toSwitchToWindow(windowId);
		log.info("iSelectTheWindow: ");
	}
	
	public void iSetTheInputWithValue(String locator, String value){
		ArrayList<String> list = readLocator(locator);		
		api.toSetInput(list.get(0), list.get(1), value);
		log.info("iSetTheInputWithValue: ");
	}

	public void iWaitForANewPageToLoad(String timeOut){
		api.toWaitforProperty(timeOut);
		log.info("iWaitForANewPageToLoad: ");
	}
	
	public void iGiveFocusToTheCurrentlySelectedWindow(){
		api.toGiveFocusToCurrentWindow();
		log.info("iGiveFocusToTheCurrentlySelectedWindow: ");
	}
	
	public void iExecuteJavascriptOnElement(String script, String locator){
		ArrayList<String> list = readLocator(locator);		
		api.toExcuteJavascript(list.get(0), list.get(1), script);
		log.info("iExecuteJavaScriptOnElement: ");
	}
	
	public void iResizeCurrentlySelectedWindowToTakeUpTheEntireScreen(){
		api.toResizeWindown();
		log.info("iResizeCurrentlySelectedWindowToTakeUpTheEntireScreen: ");
	}
	
	
	
	
}
